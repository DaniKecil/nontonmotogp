define( function ( require ) {

	"use strict";

	return {
		app_slug : 'nontonmotogp-com',
		wp_ws_url : 'https://www.nontonmotogp.com/wp-appkit-api/nontonmotogp-com',
		wp_url : 'https://www.nontonmotogp.com',
		theme : 'q-android',
		version : '0.1',
		app_title : 'NontonMotoGP.com',
		app_platform : 'android',
		gmt_offset : 7,
		debug_mode : 'off',
		auth_key : '4xipvhvtr3dbby1fomkjtsslmnftm6cx4it4k7lebvathpzg1tqsowejqykftixj',
		options : {"refresh_interval":1},
		theme_settings : [],
		addons : []
	};

});
